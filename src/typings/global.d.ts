/* eslint-disable no-undef */
declare module '@loadable/component'

declare namespace APP {
  /**
   * 约定式组件
   */
  export interface RouteFC<T = any> extends React.FC<T> {
    menu?: {
      name: string
      icon?: React.ReactNode
    }
    layout?: boolean | {}
  }
}
